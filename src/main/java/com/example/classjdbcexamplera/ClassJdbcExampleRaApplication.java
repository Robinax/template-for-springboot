package com.example.classjdbcexamplera;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClassJdbcExampleRaApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClassJdbcExampleRaApplication.class, args);
    }

}
