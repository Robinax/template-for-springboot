package com.example.classjdbcexamplera.repository.student;

import com.example.classjdbcexamplera.models.Student;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.List;

@Repository
public class StudentRepositoryImpl  implements StudentRepository{
    private String url;
    private String username;
    private String password;

    public StudentRepositoryImpl(@Value("${spring.datasource.url}")String url,
                                 @Value("${spring.datasource.username}") String username,
                                 @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public List<Student> findAll() {
        return null;
    }

    @Override
    public Student findById(Integer id) {
        Student student = null;
        String sql = "SELECT * FROM studen WHERE stud_id = ?";
        try(Connection conn = DriverManager.getConnection(url,username,password)){
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                student = new Student(
                        resultSet.getInt("stud_id"),
                        resultSet.getString("stud_name")
                );
            }

        }catch (SQLException e){
            e.printStackTrace();
        }



        return student;
    }

    @Override
    public int insert(Student object) {
        return 0;
    }

    @Override
    public int update(Student object) {
        return 0;
    }

    @Override
    public int delete(Student object) {
        return 0;
    }

    @Override
    public int deleteById(Integer integer) {
        return 0;
    }
}
