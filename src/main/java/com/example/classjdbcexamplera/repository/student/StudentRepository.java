package com.example.classjdbcexamplera.repository.student;

import com.example.classjdbcexamplera.models.Student;
import com.example.classjdbcexamplera.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<Student, Integer> {

}
